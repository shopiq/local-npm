FROM node:8

RUN mkdir /data && chown node:node /data

# see https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md
USER node
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install -g local-npm

VOLUME /data
EXPOSE 5080

CMD local-npm -d /data
